const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    let j=0;
    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        let worker = cluster.fork();
        // console.log('cluster', cluster.workers[i+1].process);

        worker.on('message', (msg)=>{
            console.log(msg.process + " and my PID: " + cluster.workers[i+1].process.pid + " and my id: " + cluster.workers[i+1].id + ' and i=' + i + ` but  j=${j}`);
        });
        worker.on('exit', (code)=>{
            console.log('Exit code: ' + code  + ' and i=' + i );
        });
    }
    j++;
    console.log('j = ', j);
    // console.log('cluster', cluster);
    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker with PID: ${worker.process.pid} died`);
    });
} else {
    let workingTime = Math.random() * 10;
    console.log(`Worker ${process.pid} started on ${workingTime} secongs`);
    setTimeout(()=>{
        process.send({process: `Process  ${process.pid} complete`});
        process.exit(0);
    }, workingTime * 1000);

}
